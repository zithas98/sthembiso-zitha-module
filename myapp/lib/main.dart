import 'package:flutter/material.dart';
import 'package:myapp/login.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Essential Deli',
      home: SplashScreen(
        seconds: 10,
        navigateAfterSeconds: const login(),
        title: Text(
          'DailyEssential App',
          style: new TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.white),
        ),
        backgroundColor: const Color.fromRGBO(100, 100, 100, 0.0),
        styleTextUnderTheLoader: new TextStyle(),
        loaderColor: Colors.white,
      ),
      theme: new ThemeData(
        primaryColor: Color.fromARGB(255, 177, 133, 108),
        primaryColorBrightness: Brightness.dark,
      ),

      //debugShowCheckedModeBanner: false,
    );
    Scaffold(
      appBar: AppBar(
        title: const Text("My Essential Deli"),
      ),
    );
  }
}
